package com.atlassian.addon.connect.autowatch.service;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class JqlProjectParserTest
{
    private final JqlProjectParser jqlProjectParser = new JqlProjectParser();

    @Test
    public void testRecognizingProjectInJql()
    {
        final Boolean parseResult = jqlProjectParser.parse("project = DEMO");
        assertThat(parseResult, is(true));
    }

    @Test
    public void testRecognizingProjectInJqlInOperator()
    {
        final Boolean parseResult = jqlProjectParser.parse("project in DEMO");
        assertThat(parseResult, is(true));
    }

    @Test
    public void testNotProjectInJqlQuery()
    {
        final Boolean parseResult = jqlProjectParser.parse("cf[10] in DEMO");
        assertThat(parseResult, is(false));
    }

    @Test
    public void testInvalidOperand()
    {
        final Boolean parseResult = jqlProjectParser.parse("Project ~ DEMO");
        assertThat(parseResult, is(false));
    }

    @Test
    public void testRecognizingCapitalizedProject()
    {
        final Boolean parseResult = jqlProjectParser.parse("PROJECT in DEMO");
        assertThat(parseResult, is(true));
    }

    @Test
    public void parseProjectWithBrackers()
    {
        final Boolean parseResult = jqlProjectParser.parse("project in (\"DEMO\")");
        assertThat(parseResult, is(true));
    }

    @Test
    public void parseProjectWithProjectName()
    {
        final Boolean parseResult = jqlProjectParser.parse("project in (\"Demonstration project\")");
        assertThat(parseResult, is(true));
    }

    @Test
    public void parseProjectIsEmpty()
    {
        final Boolean parseResult = jqlProjectParser.parse("project is EMPTY");
        assertThat(parseResult, is(true));
    }

    @Test
    public void parseProjectWithQueryFunction()
    {
        final Boolean parseResult = jqlProjectParser.parse("project in (projectsLeadByUser())");
        assertThat(parseResult, is(true));
    }

    @Test
    public void parseProjectWithQueryFunctionWithArgs()
    {
        final Boolean parseResult = jqlProjectParser.parse("project in (projectsWhereUserHasRole(\"Users\"))");
        assertThat(parseResult, is(true));
    }

    @Test
    public void parseProjectWithQueryFunctionRepetition()
    {
        final Boolean parseResult = jqlProjectParser.parse("project in (projectsWhereUserHasRole(\"Users\"), projectsLeadByUser())");
        assertThat(parseResult, is(true));
    }

    @Test
    public void parseProjectWithQueryFunctionAndNameRepetition()
    {
        final Boolean parseResult = jqlProjectParser.parse("project in (projectsWhereUserHasRole(\"Users\"), projectsLeadByUser(), \"Demonstration Project\", TEST)");
        assertThat(parseResult, is(true));
    }

    @Test
    public void parseJqlQueryWithIssuesAndProjects()
    {
        final Boolean parseResult = jqlProjectParser.parse("project in (\"Demonstration project\") AND issueKey = DEMO-123");
        assertThat(parseResult, is(true));
    }

    @Test
    public void parseJqlQueryWithoutProject()
    {
        final Boolean parseResult = jqlProjectParser.parse("issueKey = DEMO-123 AND reporter=me()");
        assertThat(parseResult, is(false));

        final Boolean secondParseResult = jqlProjectParser.parse("assignee = me() AND status = Opened AND issue in (DEMO-13)");
        assertThat(secondParseResult, is(false));
    }

    @Test
    public void parseJqlQueryWithTwoProjectQueries()
    {
        final Boolean parseResult = jqlProjectParser.parse("project in (projectsWhereUserHasRole(\"Users\"), projectsLeadByUser(), \"Demonstration Project\", DEMO) AND project = \"Demonstration Project\"");
        assertThat(parseResult, is(true));
    }

    @Test
    public void parseJqlQueryWithTwoAlternativeProjectQueries()
    {
        final Boolean parseResult = jqlProjectParser.parse("project in (projectsWhereUserHasRole(\"Users\"), projectsLeadByUser(), \"Demonstration Project\", DEMO) OR project = \"Demonstration Project\"");
        assertThat(parseResult, is(true));
    }
}
