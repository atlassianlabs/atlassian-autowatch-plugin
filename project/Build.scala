import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "atlassian-autowatch-plugin"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    javaJdbc,
    javaEbean,
    "com.atlassian.connect" % "ac-play-java_2.10" % "0.8.1",
    "log4j" % "log4j" % "1.2.15" intransitive,
    "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
    "com.google.inject" % "guice" % "3.0",
    "com.newrelic.agent.java" % "newrelic-agent" % "3.0.0",
    "com.newrelic.agent.java" % "newrelic-api" % "3.0.0",
    "com.jolbox" % "bonecp" % "0.8.0.RELEASE"
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    resolvers += "Atlassian's Maven Public Repository" at "https://maven.atlassian.com/content/groups/public",
    resolvers += "Local Maven Repository" at "file://" + Path.userHome + "/.m2/repository",
    javacOptions in Compile ++= Seq("-source", "1.7", "-target", "1.7"),
    requireJs += "main.js"
  )

}
