# Users SCHEMA

# --- !Ups
ALTER TABLE auto_watch_rule ADD invalid_execution_message TEXT;
ALTER TABLE auto_watch_rule ADD no_invalid_executions INTEGER NOT NULL DEFAULT 0;
ALTER TABLE auto_watch_rule ADD last_executed TIMESTAMP;

# --- !Downs
ALTER TABLE auto_watch_rule DROP invalid_execution_message;
ALTER TABLE auto_watch_rule DROP no_invalid_executions;
ALTER TABLE auto_watch_rule DROP last_executed;