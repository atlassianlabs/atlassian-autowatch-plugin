# Users SCHEMA

# --- !Ups
CREATE TABLE ac_host (
  id          BIGINT                 NOT NULL,
  baseurl     CHARACTER VARYING(512) NOT NULL,
  description CHARACTER VARYING(255),
  key         CHARACTER VARYING(255) NOT NULL,
  name        CHARACTER VARYING(255),
  publickey   CHARACTER VARYING(512) NOT NULL
);

CREATE SEQUENCE ac_host_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;

SELECT pg_catalog.setval('ac_host_seq', 1, FALSE);

CREATE TABLE auto_watch_rule (
  autowatch_rule_id         BIGINT                 NOT NULL,
  filter                    CHARACTER VARYING(255) NOT NULL,
  project                   CHARACTER VARYING(255) NOT NULL,
  tenant                    CHARACTER VARYING(255) NOT NULL,
  ruleowner_jira_user_id    BIGINT                 NOT NULL
);


CREATE SEQUENCE autowach_rule_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;

SELECT pg_catalog.setval('autowach_rule_seq', 1, FALSE);

CREATE TABLE jira_user (
  jira_user_id BIGINT                 NOT NULL,
  user_name    CHARACTER VARYING(255),
  tenant       CHARACTER VARYING(255) NOT NULL
);

CREATE SEQUENCE jira_user_seq START WITH 1 INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;

SELECT pg_catalog.setval('jira_user_seq', 1, FALSE);

CREATE TABLE rule_user (
  autowatch_rule_id BIGINT NOT NULL,
  jira_user_id      BIGINT NOT NULL
);

ALTER TABLE ONLY ac_host ADD CONSTRAINT ac_host_pkey PRIMARY KEY (id);
ALTER TABLE ONLY auto_watch_rule ADD CONSTRAINT auto_watch_rule_pkey PRIMARY KEY (autowatch_rule_id);
ALTER TABLE ONLY jira_user ADD CONSTRAINT jira_user_pkey PRIMARY KEY (jira_user_id);
ALTER TABLE ONLY jira_user ADD CONSTRAINT uk_jira_user UNIQUE (user_name, tenant);
ALTER TABLE ONLY ac_host ADD CONSTRAINT uk_ac_host_baseurl UNIQUE (baseurl);
ALTER TABLE ONLY ac_host ADD CONSTRAINT uk_ac_host_key UNIQUE (key);
ALTER TABLE ONLY jira_user ADD CONSTRAINT uk_jira_user_name_tenant UNIQUE (user_name, tenant);
ALTER TABLE ONLY rule_user ADD CONSTRAINT fk_rule_user_jira_user FOREIGN KEY (jira_user_id) REFERENCES jira_user (jira_user_id);
ALTER TABLE ONLY auto_watch_rule ADD CONSTRAINT fk_auto_watch_rule_jira_user FOREIGN KEY (ruleowner_jira_user_id) REFERENCES jira_user (jira_user_id);
ALTER TABLE ONLY rule_user ADD CONSTRAINT fk_rule_user_auto_watch_rule FOREIGN KEY (autowatch_rule_id) REFERENCES auto_watch_rule (autowatch_rule_id);

# --- !Downs

DROP TABLE ac_host CASCADE ;
DROP TABLE auto_watch_rule CASCADE ;
DROP TABLE jira_user CASCADE ;
DROP TABLE rule_user CASCADE ;
DROP SEQUENCE ac_host_seq;
DROP SEQUENCE autowach_rule_seq;
DROP SEQUENCE jira_user_seq;
