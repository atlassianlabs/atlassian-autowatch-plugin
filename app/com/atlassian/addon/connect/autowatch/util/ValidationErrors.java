package com.atlassian.addon.connect.autowatch.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableList;
import play.libs.Json;

public class ValidationErrors
{
    private final Iterable<ValidationError> errors;

    private ValidationErrors(Iterable<ValidationError> errors)
    {
        this.errors = errors;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public Iterable<ValidationError> getErrors()
    {
        return errors;
    }

    public JsonNode toJson()
    {
        final ObjectNode jsonNode = Json.newObject();
        final ObjectNode errorsJson = jsonNode.putObject("errors");
        for (ValidationError validationError : errors)
        {
            errorsJson.put(validationError.fieldName, validationError.errorMessage);
        }
        return jsonNode;
    }

    public static class ValidationError
    {
        private final String fieldName;
        private final String errorMessage;

        public ValidationError(String fieldName, String errorMessage)
        {
            this.fieldName = fieldName;
            this.errorMessage = errorMessage;
        }

        public String getFieldName()
        {
            return fieldName;
        }

        public String getErrorMessage()
        {
            return errorMessage;
        }
    }

    public static class Builder
    {
        private ImmutableList.Builder<ValidationError> immutableListBuilder = ImmutableList.builder();

        public Builder addError(ValidationError error)
        {
            immutableListBuilder.add(error);
            return this;
        }

        public void addErrors(ValidationErrors validationErrors)
        {
            immutableListBuilder.addAll(validationErrors.errors);
        }

        public ValidationErrors build()
        {
            return new ValidationErrors(immutableListBuilder.build());
        }
    }
}
