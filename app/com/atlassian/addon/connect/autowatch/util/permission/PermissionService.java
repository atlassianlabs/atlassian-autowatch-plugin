package com.atlassian.addon.connect.autowatch.util.permission;

import com.atlassian.connect.play.java.AcHost;

public interface PermissionService
{
    boolean hasAnyPermission(String userId, Iterable<Permission> permissions, String projectKey, AcHost host);
}
