package com.atlassian.addon.connect.autowatch.controllers;

import com.atlassian.addon.connect.autowatch.model.Issue;
import com.atlassian.addon.connect.autowatch.service.IssueEventHandler;
import com.atlassian.connect.play.java.auth.jwt.AuthenticateJwtRequest;
import com.google.inject.Inject;
import play.mvc.Controller;
import play.mvc.Result;

public class WebHookController extends Controller
{
    private final IssueEventHandler issueEventHandler;

    @Inject
    public WebHookController(IssueEventHandler issueEventHandler)
    {
        this.issueEventHandler = issueEventHandler;
    }

    @AuthenticateJwtRequest
    public Result onIssueCreated()
    {
        if (request().body().asJson().has("issue"))
        {
            final Issue issue = Issue.toIssue(request().body().asJson().get("issue"));
            issueEventHandler.handle(issue);
            return ok();
        }
        else
        {
            return badRequest();
        }
    }
}
