package com.atlassian.addon.connect.autowatch.controllers;

import play.mvc.Controller;
import play.mvc.Result;

public class QUnitController extends Controller
{
    public static Result test()
    {
        return ok(views.html.qunit.render());
    }
}
