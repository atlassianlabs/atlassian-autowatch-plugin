package com.atlassian.addon.connect.autowatch.service.email;

import com.atlassian.addon.connect.autowatch.model.Issue;
import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.connect.play.java.AcHost;

public interface EmailService
{
    void sendEmail(JiraUser recipient, AcHost acHost, Issue issue);
}
