package com.atlassian.addon.connect.autowatch.service.email;

import com.atlassian.addon.connect.autowatch.model.Issue;
import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.addon.connect.autowatch.util.JiraClient;
import com.atlassian.connect.play.java.AcHost;
import com.atlassian.fugue.Option;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import play.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;

public class EmailServiceImpl implements EmailService
{
    // TODO: change to a different service because /rest/atlassian-connect/1/email is not white-listed in the Connect scopes
    public static final String EMAIL_RESOURCE_URI = "/rest/atlassian-connect/1/email";

    private final JiraClient client;

    @Inject
    public EmailServiceImpl(JiraClient client)
    {
        this.client = client;
    }

    @Override
    public void sendEmail(final JiraUser recipient, final AcHost acHost, final Issue issue)
    {
        try
        {
            final EmailContext emailContext = new EmailContext(issue);
            final String emailBody = views.html.email.AddedAsWatcherEmail.render(emailContext).toString();
            final String emailBodyTxt = views.txt.email.AddedAsWatcherEmail.render(emailContext).toString();
            final String emailSubject = String.format("Issue %s %s was created", issue.getKey(), issue.getSummary());
            final String senderFrom = "JIRA New Issues Notifications";
            // TODO unhardcode
            final String from = getFrom(acHost);
            final RemoteEmail email = new RemoteEmail(recipient.key, emailSubject, from, senderFrom, emailBody, emailBodyTxt, "text/html");
            final StringWriter out = new StringWriter();
            new ObjectMapper().writeValue(out, email);
            final String emailJson = out.toString();
            if (StringUtils.isNotBlank(emailJson))
            {
                client.url(EMAIL_RESOURCE_URI, acHost, Option.some(recipient.key))
                        .setContentType("application/json")
                        .post(emailJson);
            }
        }
        catch (IOException e)
        {
            Logger.error("ValidationError when sending email to " + recipient.id + " about issue " + issue.getId() + " created on host " + acHost.getKey());
        }
    }

    private String getFrom(AcHost acHost)
    {
        final URI hostUri = URI.create(acHost.getBaseUrl());

        return "jira-watchers@" + hostUri.getHost();
    }
}
