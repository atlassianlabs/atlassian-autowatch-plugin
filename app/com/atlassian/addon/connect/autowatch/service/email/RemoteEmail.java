package com.atlassian.addon.connect.autowatch.service.email;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RemoteEmail
{
    @JsonProperty
    private String to;
    @JsonProperty
    private String subject;

    // optional fields
    @JsonProperty
    private String from;
    @JsonProperty
    private String fromName;
    @JsonProperty
    private String bodyAsHtml;
    @JsonProperty
    private String bodyAsText;
    @JsonProperty
    private String mimeType;

    public RemoteEmail(final String userKey, final String subject, final String from, final String fromName, final String bodyAsHtml, final String bodyAsText, final String mimeType)
    {
        this.to = userKey;
        this.subject = subject;
        this.from = from;
        this.fromName = fromName;
        this.bodyAsHtml = bodyAsHtml;
        this.bodyAsText = bodyAsText;
        this.mimeType = mimeType;
    }

    public String getTo()
    {
        return to;
    }

    public String getSubject()
    {
        return subject;
    }

    public String getFrom()
    {
        return from;
    }

    public String getFromName()
    {
        return fromName;
    }

    public String getBodyAsHtml()
    {
        return bodyAsHtml;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public String getBodyAsText()
    {
        return bodyAsText;
    }
}
