package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.model.Issue;
import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.connect.play.java.AcHost;
import com.atlassian.connect.play.java.model.AcHostModel;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;

import java.util.Date;

public interface SearchService
{
    boolean find(AutoWatchRule autoWatchRule, String issueKey, AcHost acHost);

    Either<Iterable<Issue>, Error> getAllIssuesCreatedSince(AutoWatchRule rule, Option<Date> lastScan, AcHostModel host);

    boolean validateJql(AcHost host, String projectKey, String filter, JiraUser ruleOwner);

    public class Error
    {
        private final String errorMessage;

        public Error(String errorMessage)
        {
            this.errorMessage = errorMessage;
        }

        public String getErrorMessage()
        {
            return errorMessage;
        }
    }
}
