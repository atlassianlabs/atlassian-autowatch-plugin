package com.atlassian.addon.connect.autowatch.service;

public interface NewIssuesScannerService
{
    public void synchronize();
}
