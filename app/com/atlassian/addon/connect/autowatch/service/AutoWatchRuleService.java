package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.addon.connect.autowatch.util.ValidationErrors;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;

import java.util.List;

public interface AutoWatchRuleService
{
    List<AutoWatchRule> getAllAutoWatchRules(String tenant);

    Option<AutoWatchRule> getAutoWatchRule(String tenant, Long autoWatchRule);

    Either<ValidationErrors, AutoWatchRule> createAutoWatchRule(String tenant, String jql, List<JiraUser> users, String project, JiraUser jiraUser);

    Either<ValidationErrors, AutoWatchRule> updateAutoWatchRule(AutoWatchRule autoWatchRule);

    void deleteAutoWatchRule(String tenant, Long ruleId);
}
