package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.model.Issue;
import com.atlassian.connect.play.java.AcHost;

public interface IssueWatchService
{
    void watch(AutoWatchRule rule, Issue issue, AcHost acHost);
}
