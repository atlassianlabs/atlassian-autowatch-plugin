package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.addon.connect.autowatch.util.ValidationErrors;
import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.google.common.base.Function;
import com.google.inject.Inject;

import java.util.List;

public class AutoWatchRuleServiceImpl implements AutoWatchRuleService
{
    private final JqlFilterValidator jqlFilterValidator;
    private final UserValidator userValidator;

    @Inject
    public AutoWatchRuleServiceImpl(JqlFilterValidator jqlFilterValidator, UserValidator userValidator)
    {
        this.jqlFilterValidator = jqlFilterValidator;
        this.userValidator = userValidator;
    }

    @Override
    public List<AutoWatchRule> getAllAutoWatchRules(String tenant)
    {
        return AutoWatchRule.getAllRules(tenant);
    }

    @Override
    public Option<AutoWatchRule> getAutoWatchRule(String tenant, Long autoWatchRuleId)
    {
        return AutoWatchRule.getRuleForId(autoWatchRuleId, tenant);
    }

    @Override
    public Either<ValidationErrors, AutoWatchRule> createAutoWatchRule(
            String tenant, String jql, List<JiraUser> users, String project, JiraUser jiraUser)
    {
        final AutoWatchRule autoWatchRule = new AutoWatchRule(tenant, jql, project, users, jiraUser);
        return validateAndPersist(autoWatchRule, new Function<AutoWatchRule, AutoWatchRule>()
        {
            @Override
            public AutoWatchRule apply(AutoWatchRule autoWatchRule)
            {
                return AutoWatchRule.save(autoWatchRule);
            }
        });
    }

    @Override
    public Either<ValidationErrors, AutoWatchRule> updateAutoWatchRule(AutoWatchRule autoWatchRule)
    {
        return validateAndPersist(autoWatchRule, new Function<AutoWatchRule, AutoWatchRule>()
        {
            @Override
            public AutoWatchRule apply(AutoWatchRule autoWatchRule)
            {
                return AutoWatchRule.update(autoWatchRule);
            }
        });
    }

    @Override
    public void deleteAutoWatchRule(String tenant, Long ruleId)
    {
        AutoWatchRule.delete(tenant, ruleId);
    }

    private Either<ValidationErrors, AutoWatchRule> validateAndPersist(final AutoWatchRule autoWatchRule,
            final Function<AutoWatchRule, AutoWatchRule> persistFunction)
    {
        final Option<ValidationErrors.ValidationError> jqlFilterValidationResult = jqlFilterValidator.validate(autoWatchRule);
        final Option<ValidationErrors.ValidationError> userValidationResult = userValidator.validate(autoWatchRule.users);
        if (jqlFilterValidationResult.isEmpty() && userValidationResult.isEmpty())
        {
            return Either.right(persistFunction.apply(autoWatchRule));
        }
        else
        {
            final ValidationErrors.Builder validationErrorsBuilder = ValidationErrors.builder();
            final Effect<ValidationErrors.ValidationError> errorEffect = new Effect<ValidationErrors.ValidationError>()
            {
                @Override
                public void apply(ValidationErrors.ValidationError validationError)
                {
                    validationErrorsBuilder.addError(validationError);
                }
            };

            jqlFilterValidationResult.foreach(errorEffect);
            userValidationResult.foreach(errorEffect);

            return Either.left(validationErrorsBuilder.build());
        }
    }
}
