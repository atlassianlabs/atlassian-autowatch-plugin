package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.Issue;

public interface IssueEventHandler
{
    void handle(Issue issue);
}
