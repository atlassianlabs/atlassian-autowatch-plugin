package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.model.Issue;
import com.atlassian.connect.play.java.model.AcHostModel;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.inject.Inject;
import play.Logger;
import play.Play;
import play.db.jpa.JPA;
import play.libs.F;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

public class NewIssuesScannerServiceImpl implements NewIssuesScannerService
{

    private final SearchService searchService;
    private final AutoWatchRuleService autoWatchRuleService;
    private final IssueWatchService issueWatchService;
    private final int maxInvalidExecutions;
    private final long invalidRulesRefreshInterval;
    private final Predicate<AutoWatchRule> SHOULD_FIRE = new Predicate<AutoWatchRule>()
    {
        @Override
        public boolean apply(AutoWatchRule rule)
        {
            return rule.noInvalidExecutions >= maxInvalidExecutions &&
                    (new Date().getTime() - rule.lastExecuted.getTime()) < invalidRulesRefreshInterval;

        }
    };

    @Inject
    public NewIssuesScannerServiceImpl(SearchService searchService, AutoWatchRuleService autoWatchRuleService, IssueWatchService issueWatchService)
    {
        this.searchService = searchService;
        this.autoWatchRuleService = autoWatchRuleService;
        this.issueWatchService = issueWatchService;
        this.maxInvalidExecutions = Play.application().configuration().getInt("autowatch.synchronise.max.invalid.executions", 5);
        Logger.info(String.format("Setting maximum number of invalid synchronisations to %d", maxInvalidExecutions));
        final Long invalidRilesRefreshInterval = Play.application().configuration().getLong("autowatch.invalid.synchronise.interval", TimeUnit.DAYS.toSeconds(1));
        Logger.info(String.format("Setting synchronise interval for invalid rules to %d s", invalidRilesRefreshInterval));
        this.invalidRulesRefreshInterval = TimeUnit.SECONDS.toMillis(invalidRilesRefreshInterval);
    }

    @Override
    public void synchronize()
    {

        for (final AcHostModel host : getAllHosts())
        {

            Logger.info(String.format("Synchronizing latest issues from %s", host.getBaseUrl()));
            final SyncStats stats = new SyncStats(host.getBaseUrl());

            for (final AutoWatchRule rule : autoWatchRuleService.getAllAutoWatchRules(host.getKey()))
            {
                if (SHOULD_FIRE.apply(rule))
                {
                    Logger.debug(String.format("Rule '%s' for tenant %s exceeded number of invalid executions not executing sync", rule.id, rule.tenant));
                    stats.error();
                }
                else
                {
                    final Date syncTime = new Date();
                    final Either<Iterable<Issue>, SearchService.Error> allIssuesCreatedSince = searchService.getAllIssuesCreatedSince(rule, Option.option((Date) rule.lastExecuted), host);
                    if (synchronizeRuleForHost(host, rule, allIssuesCreatedSince))
                    {
                        stats.synced();
                    }
                    else
                    {
                        stats.notSynced();
                    }
                    updateLastExecutionDate(rule, syncTime);
                }
            }
            Logger.info(stats.getMessage());
        }

    }

    private Boolean synchronizeRuleForHost(final AcHostModel host, final AutoWatchRule rule, final Either<Iterable<Issue>, SearchService.Error> issues)
    {
        return issues.fold(new Function<Iterable<Issue>, Boolean>()
                           {
                               @Nullable
                               @Override
                               public Boolean apply(Iterable<Issue> allIssuesCreatedSince)
                               {
                                   for (Issue issue : allIssuesCreatedSince)
                                   {
                                       issueWatchService.watch(rule, issue, host);
                                   }
                                   rule.resetExecutions();
                                   return Boolean.TRUE;
                               }
                           }, new Function<SearchService.Error, Boolean>()
                           {
                               @Nullable
                               @Override
                               public Boolean apply(SearchService.Error error)
                               {

                                   rule.invalidExecutionMessage = error.getErrorMessage();
                                   rule.noInvalidExecutions++;
                                   return Boolean.FALSE;

                               }
                           }
        );
    }

    private Iterable<AcHostModel> getAllHosts()
    {
        try
        {
            return JPA.withTransaction(new F.Function0<Iterable<AcHostModel>>()
            {
                @Override
                public Iterable<AcHostModel> apply() throws Throwable
                {
                    return AcHostModel.all();
                }
            });
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    private void updateLastExecutionDate(AutoWatchRule rule, Date lastScanned)
    {
        rule.lastExecuted = new Timestamp(lastScanned.getTime());
        AutoWatchRule.update(rule);
    }

    private static class SyncStats
    {
        private final String hostBaseUrl;
        private int synced;
        private int notSynced;
        private int permErrors;

        private SyncStats(final String hostBaseUrl)
        {
            this.hostBaseUrl = hostBaseUrl;
        }

        public void synced()
        {
            synced++;
        }

        public void notSynced()
        {
            notSynced++;
        }

        public void error()
        {
            permErrors++;
        }

        public String getMessage()
        {
            return String.format("Synchronized %s, temporary invalid %s, permanently invalid %s for tenant %s", synced, notSynced, permErrors, hostBaseUrl);
        }
    }
}
