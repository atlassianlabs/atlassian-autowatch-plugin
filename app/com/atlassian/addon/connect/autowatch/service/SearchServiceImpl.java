package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.model.Issue;
import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.addon.connect.autowatch.util.JiraClient;
import com.atlassian.connect.play.java.AcHost;
import com.atlassian.connect.play.java.model.AcHostModel;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.http.HttpStatus;
import play.libs.F;
import play.libs.Json;
import play.libs.WS;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchServiceImpl implements SearchService
{
    public static final Date EMPTY_DATE = new Date(0);
    private static final String SEARCH_URI = "/rest/api/2/search";
    private final JiraClient client;

    @Inject
    public SearchServiceImpl(JiraClient client)
    {
        this.client = client;
    }

    @Override
    public boolean find(AutoWatchRule autoWatchRule, String issueKey, AcHost acHost)
    {
        JsonNode searchRequest = newSearchRequest(autoWatchRule.filter, issueKey);
        F.Promise<WS.Response> responsePromise =
                client.url(SEARCH_URI, acHost, Option.some(autoWatchRule.ruleOwner.key)).post(searchRequest);

        WS.Response response = responsePromise.get();
        return response.getStatus() == HttpStatus.SC_OK && Json.parse(response.getBody()).get("total").intValue() == 1;
    }

    @Override
    public Either<Iterable<Issue>, Error> getAllIssuesCreatedSince(AutoWatchRule autoWatchRule, Option<Date> lastScan, AcHostModel host)
    {
        try
        {
            JsonNode searchRequest = newCreateSinceSearchRequest(autoWatchRule.filter, lastScan);
            F.Promise<WS.Response> responsePromise =
                    client.url(SEARCH_URI, host, Option.some(autoWatchRule.ruleOwner.key)).post(searchRequest);

            WS.Response response = responsePromise.get();
            if (response.getStatus() == HttpStatus.SC_OK)
            {
                final JsonNode jsonNode = Json.parse(response.getBody());
                if (jsonNode.has("issues"))
                {
                    final JsonNode jsonIssues = jsonNode.get("issues");
                    List<Issue> issues = Lists.newArrayList();
                    for (int i = 0; i < jsonIssues.size(); i++)
                    {
                        issues.add(Issue.toIssue(jsonIssues.get(0)));
                    }
                    return Either.left((Iterable<Issue>) issues);
                }
            }
            return Either.right(new Error(String.format("{\"message\":\"Request failed with HTTP code %d\",\"error\":%s}", response.getStatus(), response.getBody())));

        }
        catch (Exception e)
        {
            return Either.right(new Error(String.format("Exception: %s", e.getMessage())));
        }
    }

    @Override
    public boolean validateJql(AcHost tenant, String projectKey, String filter, JiraUser ruleOwner)
    {
        final WS.Response response = client.url(SEARCH_URI, tenant, Option.option(ruleOwner.key)).post(newValidateSearch(filter, projectKey)).get();
        return response.getStatus() == HttpStatus.SC_OK;
    }

    private JsonNode newValidateSearch(String filter, String projectKey)
    {
        final ObjectNode jsonNode = Json.newObject();
        jsonNode.put("jql", String.format("(project = %s) AND ( %s )", projectKey, filter));
        return jsonNode;
    }

    private JsonNode newCreateSinceSearchRequest(String filter, Option<Date> lastScan)
    {
        final ObjectNode jsonNode = Json.newObject();
        final long minutesDifference = TimeUnit.MILLISECONDS.toHours((new Date()).getTime() - lastScan.getOrElse(EMPTY_DATE).getTime());
        jsonNode.put("jql", String.format(" ( %s ) AND (created >= '%sm')", filter, minutesDifference));
        return jsonNode;
    }

    private JsonNode newSearchRequest(String filter, String issueKey)
    {
        final ObjectNode jsonNode = Json.newObject();
        jsonNode.put("jql", String.format("( %s ) AND issueKey = %s", filter, issueKey));
        return jsonNode;
    }

}
