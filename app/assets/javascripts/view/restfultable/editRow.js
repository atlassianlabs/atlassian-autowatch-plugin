define([
    "lib/jquery",
    "lib/underscore",
    "lib/backbone",
    "lib/connect",
    "lib/restfultable",
    "util/util"
],function($, _, Backbone, AP, RT, util) {
    return RT.EditRow.extend({
        initialize: function() {
            RT.EditRow.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));

            this.bind(RT.Events.FOCUS, util.resizeContainer);
        },
        serializeObject: function() {
            var data = {};

            this.$el.find(":input:not(:button):not(:submit):not(:radio):not('select[multiple]'):not([type=text].user-picker)").each(function () {

                if (this.name === "") {
                    return;
                }

                if (this.value === null) {
                    this.value = "";
                }

                data[this.name] = this.value.match(/^(tru|fals)e$/i) ?
                    this.value.toLowerCase() == "true" : this.value;
            });

            // select2 user picker override!
            this.$el.find("input[type=text].user-picker").each(function() {
                data[this.name] = $(this).select2("val");
            });

            this.$el.find("input:radio:checked").each(function(){
                data[this.name] = this.value;
            });

            this.$el.find("select[multiple]").each(function(){

                var $select = jQuery(this),
                    val = $select.val();

                if ($select.data("aui-ss")) {
                    if (val) {
                        data[this.name] = val[0];
                    } else {
                        data[this.name] = "";
                    }
                } else {

                    if (val !== null) {
                        data[this.name] = val;
                    } else {
                        data[this.name] = [];
                    }
                }
            });


            // TODO think of better way to add project key to the model
//            data.projectKey = util.getParameterByName("project_key");
            data.projectKey = AP.Meta.get("projectKey");

            return data;
        }
    });
});