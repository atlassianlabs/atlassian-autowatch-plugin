define([
    "lib/jquery",
    "lib/underscore",
    "lib/backbone",
    "lib/connect",
    "lib/restfultable",
    "util/util",
    "util/cachedRequest"
],function($, _, Backbone, AP, RT, util, CachedRequest) {
    var exports = {};
    var hostApplicationOrigin = AP.Meta.get('hostOrigin');
    var userStore = new CachedRequest({
        id: "key",
        url: "/rest/api/2/user"
    });

    exports.getRead = function () {
        return RT.CustomReadView.extend({
            render: function (self) {
                var container = $("<div></div>");
                var users = _.map(this.model.get("users"), function(user) {
                    return {
                        key: user.key
                    };
                });

                userStore.fetch(users).done(function() {
                    var users = Array.prototype.slice.call(arguments, 0);
                    var lastIndex = users.length - 1;
                    var renderedUsers = "";
                    _.each(users, function(user, idx) {
                        if (!user.isError) {
                            renderedUsers +=
                                aui.avatar.avatar({
                                    size: 'xsmall',
                                    avatarImageUrl: user.avatarUrls['16x16']
                                }) + "&nbsp;" + user.displayName +
                                    ((idx < lastIndex) ? ', ' : '');
                        } else {
                            renderedUsers += user.entityId + ((idx < lastIndex) ? ', ' : '');
                        }
                    });
                    container.html(renderedUsers);
                });
                return container;
            }
        });
    };

    exports.getEdit = function () {
        return RT.CustomEditView.extend({
            render: function (self) {
                var users = _.map(this.model.get("users"), function(user) {
                    return {
                        key: user.key
                    };
                }) || [];
                var $userPicker = $('<input type="text" class="text long user-picker" style="width: 100%;" name="users">');
                _.defer(function() {
                    function renderAvatar(avatarUrl, size) {
                        return (avatarUrl) ?
                            aui.avatar.avatar({
                                size: size,
                                avatarImageUrl: hostApplicationOrigin + avatarUrl
                            }) : "";
                    }
                    $userPicker.auiSelect2({
                        hasAvatar: true, // auiSelect2 speciffic option, adds styling needed to properly display avatars
                        multiple: true, // make the control a multi-select
                        ajax: {
                            url: "/rest/api/2/user/picker", // JIRA-relative URL to the REST end-point
                            type: "GET",
                            dataType: 'json',
                            cache: true,
                            // query parameters for the remote ajax call
                            data: function data(term) {
                                return {
                                    query: term,
                                    maxResults: 1000,
                                    showAvatar: true
                                };
                            },
                            // parse data from the server into form select2 expects
                            results: function results(data) {
                                var i, dataLength;
                                data = JSON.parse(data);

                                return {
                                    results: data.users
                                };
                            },
                            // select2 uses $.ajax as  adefault transport function so we have to override it
                            // to use AP.request for cross-origin communication
                            transport: function transport(params) {
                                AP.request({
                                    url: params.url,
                                    headers: {
                                        "Accept": "application/json"
                                    },
                                    data: params.data,
                                    success: params.success,
                                    error: params.error
                                });
                            }
                        },
                        // specify id parameter of each user entity
                        id: function id(user) {
                            return user.key;
                        },
                        // define how selected element should look like
                        formatSelection: function formatSelection(user) {
                            var avatarHtml = renderAvatar(user.avatarUrl, "xxsmall");
                            return avatarHtml + Select2.util.escapeMarkup(user.displayName);
                        },
                        // define how single option should look like
                        formatResult: function formatResult(user, container, query, escapeMarkup) {
                            // format result string
                            var resultText = user.displayName + " - (" + user.name + ")";
                            var avatarHtml = renderAvatar(user.avatarUrl, "small");
                            var higlightedMatch = [];
                            // we need this to disable html escaping by select2 as we are doing it on our own
                            var noopEscapeMarkup = function noopEscapeMarkup(s) { return s; };

                            // highlight matches of the query term using matcher provided by the select2 library
                            Select2.util.markMatch(escapeMarkup(resultText), escapeMarkup(query.term), higlightedMatch, noopEscapeMarkup);

                            // convert array to string
                            higlightedMatch = higlightedMatch.join("");

                            return avatarHtml + higlightedMatch;
                        },
                        // define message showed when there are no matches
                        formatNoMatches: function formatNoMatches(query) {
                            return "No matches found";
                        }
                    });

                    userStore.fetch(users).done(function() {

                        var selectedUsers = _.reduce(Array.prototype.slice.call(arguments, 0), function(reducedUsers, user) {
                            if (!user.isError) {
                                reducedUsers.push({
                                    key: user.key,
                                    displayName: user.displayName,
                                    avatarUrl: util.getPathFromUrl(user.avatarUrls['16x16'])
                                });
                            } else {
                                // we filter out users that were not found...
                                if (user.status != 404) {
                                    reducedUsers.push({
                                        key: user.entityId,
                                        displayName: user.entityId
                                    });
                                }
                            }
                            return reducedUsers;
                        }, []);
                        $userPicker.select2("data", selectedUsers);
                    });

                    $userPicker.on("select2-open select2-loaded", function onUserPickerOpenOrLocaded() {
                        AP.resize();
                    });
                });
                return $userPicker.wrap("<div></div>").parent();
            }
        });
    };

    return exports;
});