define([
    "lib/jquery",
    "lib/underscore",
    "lib/restfultable",
    "lib/JQLAutoComplete",
    "lib/connect"
], function($, _, RT, JQLAutoComplete, AP) {
    var exports = {};

    exports.getRead = function () {
        return RT.CustomReadView.extend({
            render: function (self) {
                var icon;

                if (this.model.get("noInvalidExecutions") > 0) {
                    icon = $(aui.icons.icon({icon: "error", useIconFont: true}));
                    this.$el
//                        .attr("title", JSON.parse(this.model.get("invalidExecutionMessage")).error.errorMessages[0])
                        .attr("title", "There was error applying this autowatch rule.")
                        .tooltip({aria:true});
                    this.$el.append(icon);
                } else {
                    icon = $(aui.icons.icon({icon: "approve", useIconFont: true}));
                    this.$el.append(icon);
                }
                this.$el.append(document.createTextNode( " " + this.model.get("filter") ));

                return this.el;
            }
        });
    };

    exports.getEdit = function() {
        return RT.CustomEditView.extend({
            render: function (self) {
                var fieldId = 'jql-filter-' + this.model.cid;
                var $html = $(
                    '<div>' +
                        '<span class="jql-indicator aui-icon aui-icon-small"></span>' +
                        '<textarea class="text jql-filter" name="filter" id="' + fieldId + '"></textarea>' +
                    '</div>'
                );
                var $jql = $html.find("textarea");
                var $errorEl = $html.find("span");
                $jql.val(this.model.get("filter")); // select currently selected

                AP.request({
                    url: "/rest/api/2/jql/autocompletedata",
                    headers: {
                        "Accept": "application/json"
                    },
                    success: function(data) {
                        data = JSON.parse(data);

                        // to disable autocomplete of project field we filter it out from the array
                        data.visibleFieldNames = _.filter(data.visibleFieldNames, function(field) {
                            return field.value !== "project";
                        });

                        var jqlAutoComplete = JQLAutoComplete({
                            field: $jql,
                            parser: JQLAutoComplete.MyParser(data.jqlReservedWords),
                            queryDelay: 0.65,
                            jqlFieldNames: data.visibleFieldNames,
                            jqlFunctionNames: data.visibleFunctionNames,
                            minQueryLength: 0,
                            allowArrowCarousel: true,
                            autoSelectFirst: false,
                            errorEl: $errorEl
                        });
                        $jql.expandOnInput();

                        jqlAutoComplete.buildResponseContainer();
                        jqlAutoComplete.parse($jql.text());
                        jqlAutoComplete.updateColumnLineCount();

                        $jql.keypress(function(event) {
                            if (jqlAutoComplete.dropdownController === null || !jqlAutoComplete.dropdownController.displayed || jqlAutoComplete.selectedIndex < 0) {
                                if (event.keyCode == 13 && !event.ctrlKey && !event.shiftKey) {
                                    event.preventDefault();
                                    jqlAutoComplete.dropdownController.hideDropdown();
                                }
                            }
                        }).bind('expandedOnInput', function() {
                                jqlAutoComplete.positionResponseContainer();
                                AP.resize(); // resize add-on container if suggestions wouldn't fit
                            }).click(function() {
                                jqlAutoComplete.dropdownController.hideDropdown();
                            });
                    }
                });

                return $html;
            }
        });
    };

    return exports;
});