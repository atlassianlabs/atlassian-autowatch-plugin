require([
    "lib/jquery",
    "lib/underscore",
    "lib/backbone",
    "lib/connect",
    "lib/restfultable",
    "util/util",
    "view/restfultable/jql",
    "view/restfultable/users",
    "view/restfultable/row",
    "view/restfultable/editRow",
    "model/restfultable/ruleModel"
],function($, _, Backbone, AP, RT, util, jqlView, usersView, AutowatchRuleRow, AutowatchRuleRowEdit, AutowatchRuleModel) {

    // DOM ready
    $(function () {
        var url = "/autoWatchRule/" + AP.Meta.get('tenant'),
            $rulesTable = $("#rules-table");

        var rulesTable = new RT({
            el: $rulesTable, // <table>
            autofocus: true, // auto focus first field of create row
            allowReorder: false, // drag and drop reordering
            model: AutowatchRuleModel,
            resources: {
                all: url, // resource to get all contacts
                self: url // resource to get single contact url/{id}
            },
            columns: [
                {
                    id: "filter",
                    fieldName: "filter",
                    header: "JQL",
                    styleClass: "field-jql",
                    readView: jqlView.getRead(),
                    editView: jqlView.getEdit(),
                    emptyText: "Enter JQL filter"
                },
                {
                    id: "users",
                    fieldName: "users",
                    header: "Users",
                    styleClass: "field-users",
                    readView: usersView.getRead(),
                    editView: usersView.getEdit(),
                    emptyText: "Enter users"
                }
            ],
            views: {
                row: AutowatchRuleRow,
                editRow: AutowatchRuleRowEdit
            },
            noEntriesMsg: "No autowatch rules have been specified.", // message to be displayed when table is empty
            fieldFocusSelector: function(name) {
                return ":input[type!=hidden][name=" + name + "], #" + name + ", .ajs-restfultable-input-" + name;
            }
        });

        AJS.$(rulesTable)
            .bind(RT.Events.INITIALIZED, util.resizeContainer)
            .bind(RT.Events.ROW_ADDED, util.resizeContainer)
            .bind(RT.Events.ROW_REMOVED, util.resizeContainer);
        $(window).scroll(util.resizeContainer);
    });
});