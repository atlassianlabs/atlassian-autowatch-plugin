define(['util/util'], function(util) {
    module("util/util", {
        setup: function() {

        }
    });

    test("getPathFromUrl", function(){
        equal( util.getPathFromUrl("http://example.org/foo/bar;param?query#frag") , "/foo/bar;param?query#frag");
        equal( util.getPathFromUrl("http://example.org/a␠b") , "/a␠b");
        equal( util.getPathFromUrl("http://example.org/a%20b") , "/a%20b");
        equal( util.getPathFromUrl("http://example.org/a%") , "/a%");
        equal( util.getPathFromUrl("http://example.org␠") , undefined);
        equal( util.getPathFromUrl("http://example.org/%<>\\^`{|}") , "/%<>\\^`{|}");
        equal( util.getPathFromUrl("http://example.org/a-umlaut-ä") , "/a-umlaut-ä");
        equal( util.getPathFromUrl("http://example.org/a-umlaut-%C3%A4") , "/a-umlaut-%C3%A4");
        equal( util.getPathFromUrl("http://example.org/a#b#c") , "/a#b#c");
        equal( util.getPathFromUrl("http://example.org/a#b␠c") , "/a#b␠c");
        equal( util.getPathFromUrl("http://a-umlaut-ä.org/") , "/");
        equal( util.getPathFromUrl("http://[FEDC:BA98:7654:3210:FEDC:BA98:7654:3210]/") , "/");
    });
});