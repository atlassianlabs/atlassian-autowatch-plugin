define([
    "lib/jquery",
    "lib/underscore",
    "lib/connect"
],function($, _, AP) {
    var defaults = {
        id: "id",
        type: "GET",
        headers: {
            "Accept": "application/json"
        },
        contentType: undefined,
        invalidateAfter: undefined // number of ms after which certain cache entry should be invalidated
    };

    var CachedRequest = function CachedRequest(options) {
        if(!options.url) {
            throw new Error("url option must be defined!");
        }

        this.options = _.extend(defaults, options);

        this.cache = {};

        return this;
    };

    var saveToCache = function (id, data) {
        this.cache[id] = data;
        if (this.options.invalidateAfter) {
            setTimeout(function() {
                delete instance.cache[data[instance.options.id]];
            }, instance.options.invalidateAfter);
        }
    };

    CachedRequest.prototype.fetch = function fetch(entities) {
        var instance = this;
        var promises = [];
        _.each(entities, function forEachEntity(entity) {
            var promise = $.Deferred();
            var entityId = entity[instance.options.id];
            promises.push(promise);

            if (instance.cache[entityId]) {
                if (instance.cache[entityId].__onItsWay) {
                    // if the object is not in the cache yet, but the request is on the way
                    instance.cache[entityId].__onItsWay.done(function requestPromiseSuccess() {
                        promise.resolve(instance.cache[entityId]);
                    });
                } else {
                    // we have a user in the cache, just return!
                    promise.resolve(instance.cache[entityId]);
                }
            } else {
                // if the user is being requested at the moment, store the promise object in the cache
                instance.cache[entityId] = { __onItsWay: promise };
                AP.request({
                    url: instance.options.url,
                    headers: instance.options.header,
                    contentType: instance.options.contentType,
                    data: entity,
                    success: function requestSuccess(data) {
                        data = JSON.parse(data);
                        instance.cache[data[instance.options.id]] = data;
                        if (instance.options.invalidateAfter) {
                            setTimeout(function() {
                                delete instance.cache[data[instance.options.id]];
                            }, instance.options.invalidateAfter);
                        }
                        promise.resolve(data);
                    },
                    error: function requestError(response) {
                        response.isError = true;
                        response.entityId = entityId;

                        instance.cache[entityId] = response;
                        if (instance.options.invalidateAfter) {
                            setTimeout(function() {
                                delete instance.cache[response[instance.options.id]];
                            }, instance.options.invalidateAfter);
                        }
                        promise.resolve(response);
                    }
                });
            }
        });

        return $.when.apply($, promises);
    };

    return CachedRequest;
});


