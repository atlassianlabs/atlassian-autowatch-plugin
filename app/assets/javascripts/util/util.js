define([
    "lib/jquery",
    "lib/underscore",
    "lib/connect"
],function($, _, AP) {
    var body = document.body;
    var documentElement = document.documentElement;

    return {
        getPathFromUrl: function (url) {
            var urlRegexp = /^[^#]*?:\/\/.*?(\/.*)$/; // path will be in the first captured group
            var result = urlRegexp.exec(url);
            return (result) ? result[1] : ((url[url.length - 1] === "/") ? "/" : undefined);
        },
        getParameterByName: function (name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return (!results) ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },
        resizeContainer: function resizeContainer() {
            var bodyHeight = Math.max(
                body.scrollHeight, documentElement.scrollHeight,
                body.offsetHeight, documentElement.offsetHeight,
                body.clientHeight, documentElement.clientHeight
            );
            if (window.innerHeight < bodyHeight) {
                AP.resize("100%", bodyHeight + 50);
            }
        }
    };

});


