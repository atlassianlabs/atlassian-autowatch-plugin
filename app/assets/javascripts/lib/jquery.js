define(function () {
    var $ = AJS.$;


    (function ($) {
        if (document.selection) {
            var fixCaretReturn = function (S) {
                return S.replace(/\u000D/g, "");
            };
            $.fn.selection = function (value) {
                var element = this[0];
                this.focus();
                if (!element) {
                    return false;
                }
                if (value == null) {
                    return document.selection.createRange().text;
                } else {
                    var scroll_top = element.scrollTop;
                    var range = document.selection.createRange();
                    range.text = value;
                    range.select();
                    element.focus();
                    element.scrollTop = scroll_top;
                }
            };
            $.fn.selectionRange = function (start, end) {
                var element = this[0];
                this.focus();
                var range = document.selection.createRange();

                if (start == null) {
                    var textAreaVal = this.val(),
                        len = textAreaVal.length,
                        dup = range.duplicate();
                    dup.moveToElementText(element);

                    dup.setEndPoint("StartToEnd", range); // move the start of dup to end of range
                    var tEnd = len - fixCaretReturn(dup.text).length;
                    dup.setEndPoint("StartToStart", range); // move to start of dup to start of range
                    var tStart = len - fixCaretReturn(dup.text).length;

                    // IE swallows the newline at the end of the selection
                    if (tEnd != tStart && textAreaVal.charAt(tEnd + 1) == "\n") {
                        tEnd += 1;
                    }
                    return {
                        end: tEnd,
                        start: tStart,
                        text: textAreaVal.substring(tStart, tEnd),
                        textBefore: textAreaVal.substring(0, tStart),
                        textAfter: textAreaVal.substring(tEnd)
                    };
                } else {
                    // first reset range to beginning of text area
                    range.moveToElementText(element);
                    range.collapse(true);

                    range.moveStart("character", start);
                    range.moveEnd("character", end - start);
                    range.select();
                }
            };
        } else {
            $.fn.selection = function (value) {
                var element = this[0];
                if (!element) {
                    return false;
                }
                if (value == null) {
                    if (element.setSelectionRange) {
                        return element.value.substring(element.selectionStart, element.selectionEnd);
                    } else {
                        return false;
                    }
                } else {
                    var scroll_top = element.scrollTop;
                    if (!!element.setSelectionRange) {
                        var selection_start = element.selectionStart;
                        element.value = element.value.substring(0, selection_start) + value + element.value.substring(element.selectionEnd);
                        element.selectionStart = selection_start;
                        element.selectionEnd = selection_start + value.length;
                    }
                    element.focus();
                    element.scrollTop = scroll_top;
                }
            };
            $.fn.selectionRange = function (start, end) {
                if (start == null) {
                    var res = {
                        start: this[0].selectionStart,
                        end: this[0].selectionEnd
                    };
                    var elementValue = this.val();
                    res.text = elementValue.substring(res.start, res.end);
                    res.textBefore = elementValue.substring(0, res.start);
                    res.textAfter = elementValue.substring(res.end);
                    return res;
                } else {
                    this[0].selectionStart = start;
                    this[0].selectionEnd = end;
                }
            };
        }
        $.fn.wrapSelection = function (before, after) {
            this.selection(before + this.selection() + (after || ""));
        };
    })($);


    // Utility method to see if any of the elements parents are fixed positioned

    $.fn.hasFixedParent = function () {
        var hasFixedParent = false;
        this.parents().each(function () {
            if (AJS.$(this).css("position") === "fixed") {
                hasFixedParent = this;
                return false;
            }
        });
        return hasFixedParent;
    };


    /**
     *
     * @module Controls
     * @requires AJS, jQuery, jQuery.moveTo
     */

    /**
     * Expands textareas upto a certain max-height, depending on the amount of content, on
     * calls to expandOnInput() and subsequent keypresses.
     *
     * Repeatedly calling expandOnInput() on the same DOM element is safe.
     *
     * <pre>
     * <strong>Usage:</strong>
     * jQuery("textarea").expandOnInput();
     * </pre>
     *
     * @class expandOnInput
     * @constuctor expandOnInput
     * @namespace jQuery.fn
     */

    (function($) {
        var eventsToListenTo = "input keyup";

        $.fn.expandOnInput = function(maxHeight) {
            var $textareas = this.filter('textarea');
            // Make sure we don't bind duplicate event handlers.
            $textareas.unbind(eventsToListenTo, setHeight).bind(eventsToListenTo, setHeight);

            //FF3.0 is especially precious when pasting into the stalker comment box. For some reason
            //it doesnt' resize on the paste rightaway.
            // Additionally, IE and FF don't scroll all the way to the bottom when a textarea got overflow
            //hidden so this scrolls to the bottom as well.  It's not perfect since it will not be right
            //if the user's pasting in the middle of some text. HTFU!!
            if($.browser.mozilla || $.browser.msie) {
                $textareas.unbind("paste", triggerKeyup).bind("paste", triggerKeyup);
            }

            $textareas.unbind("refreshInputHeight").bind("refreshInputHeight", function() {
                $(this).css('height', '');
                setHeight.call(this);
            });

            $textareas.data("expandOnInput_maxHeight", maxHeight);

            $textareas.each(function() {

                var $this = $(this);

                $this.each(function() {
                    var $this = $(this);
                    $this.data("hasFixedParent", !!$this.hasFixedParent());
                });

                // Respect initial heights for empty textareas.
                if ($this.val() !== '') {
                    setHeight.call(this);
                }
            });
            return this;
        };

        function triggerKeyup() {
            var $textarea = $(this), textarea = this;
            setTimeout(function() {
                $textarea.keyup();
                textarea.scrollTop = textarea.scrollHeight;
            }, 0);
        }

        function setHeight() {
            var $textarea = $(this),
                borderBox = ($textarea.css('boxSizing') || $textarea.css('-mozBoxSizing') ||
                    $textarea.css('-webkitBoxSizing') || $textarea.css('-msBoxSizing')) === 'border-box';

            // Workaround for IE not giving an accurate value for scrollHeight.
            // http://www.atalasoft.com/cs/blogs/davidcilley/archive/2009/06/23/internet-explorer-textarea-scrollheight-bug.aspx
            this.scrollHeight;

            var maxHeight = parseInt($textarea.css("maxHeight"), 10) || $textarea.data("expandOnInput_maxHeight") || $(window).height() - 160,
                newHeight;

            if (borderBox) {
                // FF reports scrollHeight without padding when box-sizing = border-box, so
                // can't just use outerHeight and innerHeight to calculate the new height
                var outerHeight = $textarea.outerHeight();
                newHeight = Math.max(outerHeight + this.scrollHeight - this.clientHeight, outerHeight);
            } else {
                var height = $textarea.height();
                newHeight = Math.max(this.scrollHeight - ($textarea.innerHeight() - height), height);
            }

            if (newHeight < maxHeight) {
                $textarea.css({
                    "overflow": "hidden",
                    "height": newHeight + "px"
                });
            } else {
                var cursorPosition = this.selectionStart;
                $textarea.css({
                    "overflow-y": "auto",
                    "height": maxHeight + "px"
                });

                $textarea.unbind(eventsToListenTo, setHeight);
                $textarea.unbind("paste", triggerKeyup);
                if (this.selectionStart !== cursorPosition) {
                    this.selectionStart = cursorPosition;
                    this.selectionEnd   = cursorPosition;
                }
                newHeight = maxHeight;
            }
            $textarea.trigger('expandedOnInput');

            if (!$textarea.data("hasFixedParent")) {
                var $window = $(window),
                    scrollTop = $window.scrollTop(),
                    minScrollTop = $textarea.offset().top + newHeight - $window.height() + 29;

                if (scrollTop < minScrollTop) {
                    $window.scrollTop(minScrollTop);
                }
            }

            $textarea.trigger("stalkerHeightUpdated");
        }
    })(AJS.$);




    return $;
});
